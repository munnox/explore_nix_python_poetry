{
  description = "Application packaged using poetry2nix";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    # nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    # Linked to the Official Nix Templates as an input to the flake
    official-templates.url = github:NixOS/templates;
  };

  # Built to only allow builds for one system
  outputs = { self, nixpkgs, official-templates }:
      let
        # pkgs = nixpkgs;
        system = "x86_64-linux";
        pkgs = import nixpkgs {inherit system;};
      in
      {
        # d = builtins.break;
        # t = builtins.trace official-templates;

        # Run with 'nix run .#caddyweb'
        apps.${system}.caddyweb = let
          local = ./.;
          serv = pkgs.writeShellApplication {
            # Our shell script name is serve
            # so it is available at $out/bin/serve
            name = "serve";
            # Caddy is a web server with a convenient CLI interface
            runtimeInputs = [pkgs.caddy];
            text = ''
              # Serve the current directory on port 443
              caddy file-server \
                  --domain example.com \
                  --listen 0.0.0.0:8090 \
                  --root ${local} \
                  --browse --access-log \
                  --debug
            '';
          };
        in {
          type = "app";
          # Using a derivation in here gets replaced
          # with the path to the built output
          program = "${serv}/bin/serve";
        };

        packages.${system} = {

          test_cmd_ansible = pkgs.runCommand "test"
          {buildInputs = [pkgs.ansible]; } ''
            # mkdir -p $out
            cat << EOF > $out
            #!/bin/bash
            echo helloworld
            EOF
            chmod +x $out
            export ANSIBLE_HOME=./
            PS4='+ Line $(expr $LINENO + 4): '
            set -o xtrace
            ansible --version >> $out
            set +o xtrace
            echo Hello world 
          '';

          # Run with 'nix run .#test_shell_ansible
          test_shell_ansible = pkgs.writeShellApplication {
            name = "test_shell_ansible";
            runtimeInputs = [pkgs.ansible];
            text = ''
              echo "Running test_ansible_ping home: '$HOME' Script arguments: '$*' "
              echo "Show the Ansible version"
              ansible --version
              echo "Show an anisble ping on the localhost"
              ansible localhost -m ping
            '';
          };

          # Run with 'nix run .#der --print-build-logs'
          der = pkgs.stdenv.mkDerivation
            {
              src = ./.;
              # The package name without the version is "rust-hello"
              name = "test script";
              inherit system;
              nativeBuildInputs = [
                # pkgs.cargo
                pkgs.ansible
              ];
              buildPhase = ''
                #cargo build --release
                echo "Hello World"
                ansible --version
                echo Phases: $phases
                ls
                build_variable="Variable set in build phase"
                touch test
              '';
              # The binary output is at $out/bin/test
              installPhase = ''
                mkdir -p $out/bin
                echo Print var $build_variable
                cp test $out/bin/test
                #chmod +x $out
              '';
            };
        };

        templates = {
          explore-poetry = {
            path = ./python-poetry-example;
            description = "Local Python and Poetry examples exploration for coommand and environments";
          };
          simple-flake = {
            path = ./simple-flake;
            description = "Local template with a simple flake for all system";
          };
        };
        # // official-templates.templates;
        # The above allow the offical templates to be added to this flake
      };
}
