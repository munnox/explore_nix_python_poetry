# Simple flake targeting multiple systems
{
  description = "My example Nix flake for multiple systems";
  
  inputs.flake-utils.url = "github:numtide/flake-utils";
  
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        debug_args = {
            msg = "Hello world";
        };

        test_shell_ansible = pkgs.writeShellApplication {
        name = "test_shell_ansible";
        runtimeInputs = [pkgs.ansible];
        # checkScript = false;
        text = ''
            echo "Running test_ansible_ping home: '$HOME' Script arguments: '$*' "
            echo "Show the Ansible version"
            ansible --version
            echo "Show an anisble ping on the localhost"
            ansible localhost -m debug -a ${pkgs.lib.strings.escapeShellArg (builtins.toJSON debug_args)}
            ansible localhost ${pkgs.lib.strings.escapeShellArg "$@"}
        '';
        };

        # ---- Ansible testing ------
        # Define a simple inventory with a localhost hosts
        # Used in the tests below
        inventory_yaml = pkgs.writeText "inv.yaml" ''
        all:
          hosts:
            localhost:
              ansible_connection: local
        '';

        # Define a test playbook to run on the localhost to
        # ping and print a debug tasks
        playbook_yaml = pkgs.writeText "test_playbook.yaml" ''
        - name: Test Playbook
          hosts: localhost
          tasks:
          - ping:
          - debug:
              msg: "Ping and Hello worlds"
        '';

        # Copy the test playbook defined above into the local directory
        get_playbook = pkgs.writeScriptBin "get_playbook" ''
          echo "Copying by force the test playbook"
          cp -f ${playbook_yaml} ./${playbook_yaml.name}
        '';

        # Define the basic inventories
        all_inventories = "-i ${inventory_yaml}";

        # Run the Ansible test playbook with the inventory defined above
        # Load a .env if there is one
        run_playbook_test = pkgs.writeScriptBin "run_playbook_test" ''
          echo "Running test playbook"
          [[ -f .env ]] && source .env
          ${pkgs.ansible}/bin/ansible-playbook ${all_inventories} ${playbook_yaml}
        '';
        
        # From home_config repo or unix_config deploy
        run_playbook = pkgs.writeScriptBin "run_playbook" ''
          echo "Running playbook: $@"
          [[ -f .env ]] && source .env
          ${pkgs.ansible}/bin/ansible-playbook ${all_inventories} $@
        '';

        # Originally from https://www.ertt.ca/nix/shell-scripts/
        test_shell_script = pkgs.writeShellScriptBin "test_shell_script" ''
          DATE="$(${pkgs.ddate}/bin/ddate +'the %e of %B%, %Y')"
          ${pkgs.cowsay}/bin/cowsay Hello, world! Today is $DATE.
        '';
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = [
            pkgs.hello
            test_shell_ansible
            test_shell_script
            get_playbook
            run_playbook_test
            run_playbook
          ];
          shellHook = ''
          PS1="NIX-$PS1"
          '';
        };
      }
    );
}