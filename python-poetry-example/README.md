# Exploration of Nix Python and Poetry for development

Used https://github.com/nix-community/poetry2nix/#using-the-flake

and the main flake command

```
nix flake init --template github:nix-community/poetry2nix
```

This created:

```
|-- README.md
|-- app
|   |-- __init__.py
|   `-- __pycache__
|       `-- __init__.cpython-311.pyc
|-- flake.lock
|-- flake.nix
|-- poetry.lock
`-- pyproject.toml

3 directories, 7 files
```

I added the poetry script in `pyproject.toml` and the `def main()` main function `app/__init__.py`.

To build and run the python program in here:

```bash
# Development enviroment
nix develop
# Then the poetry app needs to be run with
poetry run app
```

OR (the repo needed staging to allow the following to work)


```bash
nix shell
# then the app can be run with
app
```