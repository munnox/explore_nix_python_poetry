{
  description = "Application packaged using poetry2nix";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    # nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    official-templates.url = github:NixOS/templates;
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix, official-templates }:
    {
      templates = {
        copy = {
          path = ./.;
          description = "Use this folder as a template";
        };
      } // official-templates.templates;
      checks.x86_64-linux.math = with nixpkgs.legacyPackages.x86_64-linux;
        runCommand "math-test" {} ''
          test $(( 1 + 1 )) -eq  3
          touch $out
        '';
    } //
    flake-utils.lib.eachDefaultSystem (system:
      let
        # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
        pkgs = nixpkgs.legacyPackages.${system};
        newpkgs = self.packages.${system};
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication;
      in
      {
        packages = {
          myapp = mkPoetryApplication { projectDir = self; };
          hello = pkgs.hello;
          env = pkgs.buildEnv {
            name="combined-env";
            paths = [
              newpkgs.myapp
              pkgs.ansible
              newpkgs.hello
            ];
          };
          # Used via nix shell .#combine
          # path command NOT available on nix develop .# combine
          combine = pkgs.symlinkJoin {
            name = "combine";
            paths = [
              newpkgs.myapp
              pkgs.ansible
              pkgs.hello
              pkgs.tree
            ];
            # The post build doesn't work or run visibily
            preBuild = ''
              echo App Dev Combined
            '';
          };
          # Run the text under nix run .#sh
          sh = pkgs.writeShellApplication {
            name = "combineshell";
            runtimeInputs = [
              newpkgs.myapp
              pkgs.ansible
              pkgs.hello
              pkgs.tree
            ];
            # This works and the language seems to be set before running ansible
            text = ''
              echo App Dev Combined "''${@}"
              export LANG=C.UTF-8
              export LC_ALL=C.UTF-8
              app
              ansible --version
              ansible "''${@}"
            '';
          };
          # default = newpkgs.myapp;
          # To run with nix develop .#dev
          dev = 
            let
              test = "simple";
              debugtrace = x: pkgs.lib.traceSeq x x;
              r = debugtrace test; #newpkgs.myapp;
              t = builtins.trace newpkgs.myapp;
            in
            pkgs.mkShell {
            nativeBuildInputs = [
              # pkgs.python311
              # newpkgs.myapp
              newpkgs.env
            ];
            # buildInputs = [
            #   # pkgs.python311
            #   pkgs.ansible
            #   # pkgs.poetry
            #   # newpkgs.myapp
            # ];
            # inputsFrom = [ newpkgs.myapp ];
            packages = newpkgs.myapp.nativeBuildInputs ++ [
              # pkgs.python311
              # pkgs.poetry
              # pkgs.ansible
              # newpkgs.myapp
            ];
            shellHook = ''
              echo "hello world"
              #export LANG=C.UTF-8
              #export LC_ALL=C.UTF-8
              export PS1="NIX-$PS1"
              # Debugging and exploreing derivations and their attributes
              echo "${r} ${newpkgs.myapp.LANG}"
              echo "${builtins.toString (builtins.attrNames newpkgs.myapp)}"
              echo ""
              echo "${builtins.toString newpkgs.twcli.args}"
              echo ""
              echo "${builtins.toString newpkgs.twcli.out}"
              echo ""
              echo "${builtins.toString newpkgs.twcli.outputName}"
              echo ""
              echo "${builtins.toString newpkgs.twcli.outputs}"
              echo ""
              echo "${builtins.toString newpkgs.twcli.pythonPath}"
              echo ""
              echo "${builtins.toString newpkgs.twcli.dist}"
              echo ""
              echo "${builtins.toString newpkgs.twcli.buildInputs}"
              echo ""
              echo "${builtins.toString newpkgs.twcli.nativeBuildInputs}"
            '';
          };

          fallback = pkgs.mkShell {
            # packages = [ poetry2nix.packages.${system}.poetry ];
            # inputsFrom = [ newpkgs.dev ];
            packages = [
              pkgs.poetry
              pkgs.ansible
              # newpkgs.myapp
            ];
          };

          develop = pkgs.mkShell {
            # packages = [ poetry2nix.packages.${system}.poetry ];
            # inputsFrom = [ newpkgs.dev ];
            packages = [
              # pkgs.poetry
              # pkgs.ansible
              newpkgs.myapp
            ];
          };
          # Used by running nix build .#docker
          # loaded by running ./result | docker load
          # started by running docker run -it --rm <result of load>
          docker = pkgs.dockerTools.streamLayeredImage {
            name = "app";
            contents = [ newpkgs.myapp ];
            config.Cmd = [ "app" ];
          };
        };

        # devShells.dev =  newpkgs.dev;
        # devShells.default = newpkgs.myappdev;

        # templates = {
        #   python-local = {
        #     path = ./.;
        #     description = "This Repo to be used as a template";
        #   };

        # };
        # // official-templates.templates;
      });
}
